THIS IS WORK IN PROGRESS!

The main page of the Open Cycling Computer: http://opencyclingcomputer.eu

Code can be run with
```
cd code
./occ.sh
```
and no longer runs in full simulation mode. GTK simulation mode is being implemented. The real hardware (raspberry pi & piTFT) is recommended.

GUI diagram [WIP]:
![GUI](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/Documentation/OCC_GUI.png)

Wirign block diagram
![block diagram](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/Documentation/block_diagram.png?raw=true)

Documentation: http://opencyclingcomputer.eu/Documentation/

Some info about the code can be found here:
http://blog.firszt.eu/index.php?tag/OpenCyclingComputer

Screenshots:

![DEMO 1](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_1.png?raw=true)
![DEMO 2](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_2.png?raw=true)
![DEMO 3](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_3.png?raw=true)
![DEMO 4](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_4.png?raw=true)
![DEMO 5](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_5.png?raw=true)
![DEMO 6](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_6.png?raw=true)
![DEMO 7](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_7.png?raw=true)
![DEMO 8](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_8.png?raw=true)
![DEMO 9](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_9.png?raw=true)
![DEMO 10](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_10.png?raw=true)
![DEMO 11](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_11.png?raw=true)
![DEMO 12](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_12.png?raw=true)
![DEMO 13](https://gitlab.com/PrzemoF/Open-Cycling-Computer/raw/master/demo_screenshoots/demo_13.png?raw=true)


Licence CC-SA-BY

P.S. If you feel like trying the code, but you have some problems with that... I'll be very happy to help!
